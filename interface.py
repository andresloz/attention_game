#!/usr/bin/env python
#*- coding: utf-8 -*-

from os import path, getcwd
from time import sleep
from random import randint, shuffle

# tk libraries
from Tkinter import *
import tkMessageBox as tkMsg
import tkFont

NAME = "Jeu Attention Test"
VERSION = "0.0.1 2016"
AUTHOR = "Andres Lozano a.k.a Loz"
COPYRIGHT = "Copyleft: This is a free work, you can copy, distribute, and modify it under the terms of the Free Art License http://artlibre.org/licence/lal/en/"
URL = "http://hyperficiel.com"

class Interface:
	def __init__(self):
		# roots dialogs
		self.openRoot = getcwd()
		self.saveRoot = None
		self.fileName = None
		self.canvasSize = (1024,680)
		self.spriteSizes = (103,103)
		self.animationLoops = 250
		self.delay = 0.05
		self.score = 0
		self.plays = 0
		self.static = 0
		self.moveSprites = 1
		self.animalsNames = {
			"0":"canard","1":"mouton","2":"cochon","3":"chien","4":"grenouille",
			"5":"cheval","6":"tigre","7":"âne","8":"oiseau","9":"chèvre",
			"10":"coq","11":"vache","12":"élephant","13":"singe","14":"lion",
			"15":"jaguar","16":"zèbre","17":"serpent","18":"ours","19":"loup",
			"20":"giraffe","21":"rhinoceros","22":"renne","23":"zébu"
		}
		self.filesDir = "animals/gif/"
		self.animalPathName = []
		self.animalChoosedNum = 0
		self.animalsSelection = []
		self.animalQty = 0
		self.placeOrder = 0
		self.winParamsPos = None
		self.winChoicePos = None
		self.winCanSizesPos = None
		
		# window vars and draw interface
		self.tkRoot = Tk()
		self.defaultBgColor = self.tkRoot.cget("bg")
		self.tkRoot.title("Attention")
		self.fontFamily = "Arial"
		self.fontSize = 10
		self.monoFontSize = 9
		self.customFont = tkFont.Font(family=self.fontFamily, size=self.fontSize)
		self.customFontMini = tkFont.Font(family=self.fontFamily, size=self.fontSize-2)
		self.customFontBold = tkFont.Font(family=self.fontFamily, size=self.fontSize, weight="bold")
		self.customMonoFont = tkFont.Font(family="courier", size=self.monoFontSize)

		# draw interface
		self.drawInterface()
		# load interface
		self.tkRoot.mainloop()
		
	# actions		
	def playAnimals(self, animals=11, static=0):
		if static:
			self.static = 1
		else:
			self.static = 0
			
		self.plays += 1
		self.canvasPreview.delete("all")
		self.drawZones()
		self.addAnimals(animals=animals)
		self.loopAnimalAnimation(loops=self.animationLoops, delay=self.delay)
		self.stopAnimalAnimation()
		
	def drawZones(self):
		position = self.defineZonesVals()
		
		self.canvasPreview.create_rectangle(position["A"], position["E"], position["B"], position["F"], fill="#ffffff", width=0) 
		self.canvasPreview.create_rectangle(position["B"], position["E"], position["C"], position["F"], fill="#dddddd", width=0)
		self.canvasPreview.create_rectangle(position["C"], position["E"], position["D"], position["F"], fill="#bbbbbb", width=0)
		
		self.canvasPreview.create_rectangle(position["A"], position["F"], position["B"], position["G"], fill="#eeeeee", width=0)
		self.canvasPreview.create_rectangle(position["B"], position["F"], position["C"], position["G"], fill="#cccccc", width=0)
		self.canvasPreview.create_rectangle(position["C"], position["F"], position["D"], position["G"], fill="#aaaaaa", width=0)
		return
	
	def addAnimals(self, animals=11):
		self.animalImages = []
		self.animalSprites = []
		self.animalSpriteSpeeds = []
		self.animalPathName = []
		self.animalsSelection = []
		self.animalQty = animals
		numbers = range(24)
		for i in numbers: # add file to array
			img = self.filesDir+"file-"+str(i).zfill(3)+".gif"
			img = path.join(self.openRoot,img)
			img = path.normpath(img)
			self.animalPathName.append(img)
			self.animalImages.append(PhotoImage(file = img))
			
		shuffle(numbers)
		
		if self.placeOrder == 1:
			col = 0
			line = 0
			for i in range(animals): # choose some sprites
				num = numbers.pop()
				self.animalsSelection.append(num)
				if col < 4:
					x = 50+(100*col)
					y = 50+(100*line)
					col += 1
				else:
					x = 50+(100*col)
					y = 50+(100*line)
					col = 0
					line += 1
				
				self.animalSprites.append(self.canvasPreview.create_image(x,y,image=self.animalImages[num]))
				self.animalSpriteSpeeds.append([5,5])
		else:
			for i in range(animals): # choose some sprites
				num = numbers.pop()
				self.animalsSelection.append(num)
				x = randint(50,self.canvasSize[0]-50)
				y = randint(50,self.canvasSize[1]-50)
				self.animalSprites.append(self.canvasPreview.create_image(x,y,image=self.animalImages[num]))
				self.animalSpriteSpeeds.append([randint(-10,+10),randint(-10,+10)])
		
	def loopAnimalAnimation(self, loops=100, delay=0.01):
		self.moveSprites = 1
		i = 0
		while self.moveSprites and i < loops:
			i += 1
			percent = loops/100.0
			self.scoreLabel.configure(text="Temps restant "+str(100-int(i/percent))+"%")
			if not self.static:self.animalMoveOneStep(spriteSizes=self.spriteSizes)
			sleep(delay)
			self.canvasPreview.update()
		
		return

	def animalMoveOneStep(self, spriteSizes=(100,100)):
		frameBorderWidth = spriteSizes[0]/2
		frameBorderHeight = spriteSizes[1]/2
		for i in range(len(self.animalSprites)):
			x, y = self.canvasPreview.coords(self.animalSprites[i])
			if x > self.canvasSize[0]-frameBorderWidth	: self.animalSpriteSpeeds[i][0] = -self.animalSpriteSpeeds[i][0]
			if x < frameBorderWidth						: self.animalSpriteSpeeds[i][0] = 5
			if y > self.canvasSize[1]-frameBorderHeight	: self.animalSpriteSpeeds[i][1] = -self.animalSpriteSpeeds[i][1]
			if y < frameBorderHeight					: self.animalSpriteSpeeds[i][1] = 5
			self.canvasPreview.move(self.animalSprites[i],self.animalSpriteSpeeds[i][0],self.animalSpriteSpeeds[i][1])
			
		return
			
	def stopAnimalAnimation(self, event=None):
		if not self.animalSprites: return
		self.moveSprites = 0
		animalChoosed = randint(0,self.animalQty-1)
		self.animalSelectedCoords = self.canvasPreview.coords(self.animalSprites[animalChoosed])
		self.animalChoosedNum = self.animalsSelection[animalChoosed]
		
		for i in range(self.animalQty):
			self.canvasPreview.delete(self.animalSprites[i])
			
		self.animalSprites = [] # reset sprites array
		self.drawChoiceDialog(message="De quel coté se trouvait '"+self.animalsNames[str(self.animalChoosedNum)]+"' avant de disparaître ?", animalNum=self.animalChoosedNum)
		return
			
	def showGamerScore(self):
		self.scoreLabel.configure(text="Vous avez réussi "+str(self.score)+" fois sur "+str(self.plays)+" essais")
		return
		
	def defineZonesVals(self):
		posA = 0
		posB = self.canvasSize[0]/3
		posC = (self.canvasSize[0]/3) * 2
		posD = (self.canvasSize[0]/3) * 3
		posE = 0
		posF = self.canvasSize[1]/2
		posG = self.canvasSize[1]
		gridVals = {
			"A":posA,"B":posB,"C":posC,"D":posD,
			"E":posE,"F":posF,"G":posG
		}		
		return gridVals
	
	def setMaxCanvasSize(self):
		screen_width = self.tkRoot.winfo_screenwidth()
		screen_height = self.tkRoot.winfo_screenheight()
		w,h = self.canvasSize
		if screen_width < w:
			w = screen_width-2
		if screen_height < h+50:
			h = screen_height - 50
			
		self.canvasSize = (w,h)
		
	# infos
	def quitInterface(self, event=None):
		self.tkRoot.destroy()
		
	def about(self, event=None):
		msg = "\n".join([NAME,VERSION,AUTHOR,URL,COPYRIGHT])
		tkMsg.showinfo("About", msg)
		
	# tk message dialogs
	def winError(self, m=""):
		tkMsg.showerror("Error", m)
		
	def winCancel(self, m=""):
		tkMsg.showinfo("Cancel", m)
		
	def winDone(self, m=""):
		tkMsg.showinfo("Done", m)
			
	# -------------------------------- draw interfaces
	def drawWinCanSizes(self):
		try:
			self.winCanSizes.destroy()
		except:
			pass
		
		self.winCanSizes = Toplevel(self.tkRoot)
		self.winCanSizesXLabel = Label(self.winCanSizes, font=self.customFontBold, text="Largeur")
		self.winCanSizesX = Scale(self.winCanSizes, from_=280, to =1440, length=200, showvalue=1, font=self.customFont, orient=HORIZONTAL)
		self.winCanSizesX.set(self.canvasSize[0])
		self.winCanSizesX.bind("<ButtonRelease-1>", self.updateCanSizes)
		self.winCanSizesYLabel = Label(self.winCanSizes, font=self.customFontBold, text="Hauteur")
		self.winCanSizesY = Scale(self.winCanSizes, from_=192, to =1280, length=200, showvalue=1, font=self.customFont, orient=HORIZONTAL)
		self.winCanSizesY.set(self.canvasSize[1])
		self.winCanSizesY.bind("<ButtonRelease-1>", self.updateCanSizes)
		self.winCanSizesResetVals = Button(self.winCanSizes, font=self.customFontBold, text="Par défaut", command=self.resetCanSizes)
		self.winCanSizesClose = Button(self.winCanSizes, font=self.customFontBold, text="Fermer", command=self.closeCanSizes)
		
		mainFrame = [
			# grid representation r: row, c: column, rs: rowspan, cs: columnspan
			# widget						r  c  rs cs sticky
			[self.winCanSizesXLabel, 		0, 0, 1, 1, N+W],
			[self.winCanSizesX, 			0, 1, 1, 1, N+W+E],
			[self.winCanSizesYLabel, 		1, 0, 1, 1, N+W],
			[self.winCanSizesY, 			1, 1, 1, 1, N+W+E],
			[self.winCanSizesResetVals, 	2, 0, 1, 1, N+W+E+S],
			[self.winCanSizesClose, 		2, 1, 1, 1, N+W+E+S]
		]
		for w in mainFrame:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
			
		self.winCanSizes.withdraw() # hide win
		self.winCanSizes.protocol("WM_DELETE_WINDOW", self.closeCanSizes)	
		self.winCanSizes.title("Paramètres écran")
		if not self.winCanSizesPos: # set position
			self.winCanSizes.update() # update win
			wRoot = []; wCanSizes = [] # geometry values of elements
			[wRoot.extend(x) for x in [x.split("+") for x in [x for x in self.tkRoot.geometry().split("x")]]]
			[wCanSizes.extend(x) for x in [x.split("+") for x in [x for x in self.winCanSizes.geometry().split("x")]]]
			
			firstTime = wCanSizes[0]+"x"+wCanSizes[1]+"+"+str(int(wRoot[2]))+"+"+str(int(wRoot[3]))
			self.winCanSizesPos = firstTime
		
		self.winCanSizes.geometry(self.winCanSizesPos)
		self.winCanSizes.deiconify() # show win
		self.winCanSizes.mainloop()
		
	def updateCanSizes(self, event=None):
		self.canvasSize = (self.winCanSizesX.get(),self.winCanSizesY.get())
		self.canvasPreview.configure(width=self.canvasSize[0], height=self.canvasSize[1]) # add configure to remain original size and make it scrolling
		self.drawZones()
		return
		
	def resetCanSizes(self, event=None):
		self.canvasSize = (1024,680)
		self.canvasPreview.configure(width=self.canvasSize[0], height=self.canvasSize[1]) # add configure to remain original size and make it scrolling
		self.drawZones()
		return
		
	def closeCanSizes(self, event=None):
		self.winCanSizesPos = self.winCanSizes.geometry()
		try:
			self.winCanSizes.destroy()
		except:
			pass
		
	def drawWinParams(self):
		try:
			self.winParams.destroy()
		except:
			pass
		
		self.winParams = Toplevel(self.tkRoot)
		self.winParamsloopsLabel = Label(self.winParams, font=self.customFontBold, text="Temps-/+")
		self.winParamsloops = Scale(self.winParams, from_=0, to =500, length=200, showvalue=1, font=self.customFont, orient=HORIZONTAL)
		self.winParamsloops.set(self.animationLoops)
		self.winParamsloops.bind("<ButtonRelease-1>", self.updateParams)
		self.winParamsSpeedLabel = Label(self.winParams, font=self.customFontBold, text="Vitesse -/+")
		self.winParamsSpeed = Scale(self.winParams, from_=1, to=100, length=200, showvalue=1, font=self.customFont, orient=HORIZONTAL)
		self.winParamsSpeed.set(100 - (self.delay*1000))
		self.winParamsSpeed.bind("<ButtonRelease-1>", self.updateParams)
		self.winParamsResetVals = Button(self.winParams, font=self.customFontBold, text="Par défaut", command=self.resetParams)
		self.winParamsClose = Button(self.winParams, font=self.customFontBold, text="Fermer", command=self.closeWinParams)
		
		mainFrame = [
			# grid representation r: row, c: column, rs: rowspan, cs: columnspan
			# widget						r  c  rs cs sticky
			[self.winParamsloopsLabel, 		0, 0, 1, 1, N+W],
			[self.winParamsloops, 			0, 1, 1, 1, N+W+E],
			[self.winParamsSpeedLabel, 		1, 0, 1, 1, N+W],
			[self.winParamsSpeed, 			1, 1, 1, 1, N+W+E],
			[self.winParamsResetVals, 		2, 0, 1, 1, N+W+E+S],
			[self.winParamsClose, 			2, 1, 1, 1, N+W+E+S]
		]
		for w in mainFrame:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
			
		self.winParams.withdraw() # hide win
		self.winParams.protocol("WM_DELETE_WINDOW", self.closeWinParams)	
		self.winParams.title("Paramètres")
		
		if not self.winParamsPos: # set position
			self.winParams.update() # update win
			wRoot = []; wParams = [] # geometry values of elements
			[wRoot.extend(x) for x in [x.split("+") for x in [x for x in self.tkRoot.geometry().split("x")]]]
			[wParams.extend(x) for x in [x.split("+") for x in [x for x in self.winParams.geometry().split("x")]]]
			
			firstTime = wParams[0]+"x"+wParams[1]+"+"+str(int(wRoot[2]))+"+"+str(int(wRoot[3]))
			self.winParamsPos = firstTime
		
		self.winParams.geometry(self.winParamsPos)
		self.winParams.deiconify() # show win
		self.winParams.mainloop()
	
	def updateParams(self, event=None):
		self.animationLoops = self.winParamsloops.get()
		self.delay = (100 - self.winParamsSpeed.get()) / 1000.0
		return
		
	def resetParams(self, event=None):
		self.animationLoops = 250
		self.winParamsloops.set(self.animationLoops)
		self.delay = 0.09
		self.winParamsSpeed.set(self.delay*1000)
		return
		
	def closeWinParams(self, event=None):
		self.winParamsPos = self.winParams.geometry()
		try:
			self.winParams.destroy()
		except:
			pass
			
	def drawChoiceDialog(self, message="", animalNum=1):
		try:
			self.winChoice.destroy()
		except:
			pass
			
		self.winChoice = Toplevel(self.tkRoot)
		self.winChoice.configure(background=self.defaultBgColor)
		self.winChoiceLabel = Label(self.winChoice, font=self.customFontBold, text=message)
		self.canvasWinChoice = Canvas(
			self.winChoice, width=self.spriteSizes[0], height=self.spriteSizes[1], borderwidth=1, 
			background="#ffffff"
		)
		img = self.animalPathName[animalNum]
		self.animalImage = PhotoImage(file = img)
		self.canvasWinChoice.create_image(self.spriteSizes[0]/2,self.spriteSizes[1]/2,image=self.animalImage)
		self.choiceSide = IntVar()
		self.choiceSide.set(0)
		self.winChoiceRadioButtonUpLeft = Radiobutton(self.winChoice, text="haut gauche", variable=self.choiceSide, font=self.customFontBold, value=1, background="#ffffff", height=3)
		self.winChoiceRadioButtonUpCenter = Radiobutton(self.winChoice, text="haut centre", variable=self.choiceSide, font=self.customFontBold, value=2, background="#dddddd", height=3)
		self.winChoiceRadioButtonUpRight = Radiobutton(self.winChoice, text="haut droit", variable=self.choiceSide, font=self.customFontBold, value=3, background="#bbbbbb", height=3)
		self.winChoiceRadioButtonBottomLeft = Radiobutton(self.winChoice, text="bas gauche", variable=self.choiceSide, font=self.customFontBold, value=4, background="#eeeeee", height=3)
		self.winChoiceRadioButtonBottomCenter = Radiobutton(self.winChoice, text="bas centre", variable=self.choiceSide, font=self.customFontBold, value=5, background="#cccccc", height=3)
		self.winChoiceRadioButtonBottomRight = Radiobutton(self.winChoice, text="bas droit", variable=self.choiceSide, font=self.customFontBold, value=6, background="#aaaaaa", height=3)
		self.jouerButton = Button(self.winChoice, font=self.customFontBold, text="jouer", command=self.winChoiceGet)
		
		mainFrameGrid = [
			# widget								r  c  rs cs sticky
			[self.winChoiceLabel, 					0, 0, 1, 3, W+S],
			[self.canvasWinChoice, 					1, 0, 1, 3, None],
			[self.winChoiceRadioButtonUpLeft, 		2, 0, 1, 1, N+W+E+S],
			[self.winChoiceRadioButtonUpCenter, 	2, 1, 1, 1, N+W+E+S],
			[self.winChoiceRadioButtonUpRight, 		2, 2, 1, 1, N+W+E+S],
			[self.winChoiceRadioButtonBottomLeft, 	3, 0, 1, 1, N+W+E+S],
			[self.winChoiceRadioButtonBottomCenter, 3, 1, 1, 1, N+W+E+S],
			[self.winChoiceRadioButtonBottomRight, 	3, 2, 1, 1, N+W+E+S],
			[self.jouerButton, 						4, 0, 1, 3, N+W+E+S]
		]
		for w in mainFrameGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
		
		self.winChoice.withdraw() # hide win
		self.winChoice.protocol("WM_DELETE_WINDOW", self.closeWinChoice)	
		self.winChoice.title("Jouer")
		self.winChoice.update() # update win
		wRoot = []; wChoice = [] # geometry values of elements
		[wRoot.extend(x) for x in [x.split("+") for x in [x for x in self.tkRoot.geometry().split("x")]]]
		[wChoice.extend(x) for x in [x.split("+") for x in [x for x in self.winChoice.geometry().split("x")]]]
		x = int(wRoot[2])+(int(wRoot[0]) - int(wChoice[0]))/2
		y = int(wRoot[3])+(int(wRoot[1]) - int(wChoice[1]))/2
		self.winChoicePos = wChoice[0]+"x"+wChoice[1]+"+"+str(x)+"+"+str(y)
		
		self.winChoice.geometry(self.winChoicePos)
		self.winChoice.deiconify() # show win
		self.winChoice.mainloop()
		
	def winChoiceGet(self):
		animalx, animaly = self.animalSelectedCoords
		position = self.defineZonesVals()
		animal = 0
		if position["E"] < animaly < position["F"]:
			if position["A"] < animalx < position["B"]: animal = 1
			if position["B"] < animalx < position["C"]: animal = 2
			if position["C"] < animalx < position["D"]: animal = 3
			
		if position["F"] < animaly < position["G"]:
			if position["A"] < animalx < position["B"]: animal = 4
			if position["B"] < animalx < position["C"]: animal = 5
			if position["C"] < animalx < position["D"]: animal = 6
			
		img = self.animalPathName[self.animalChoosedNum]
		self.imageSolution = PhotoImage(file = img)
		self.canvasPreview.create_image(animalx, animaly,image=self.imageSolution)
		self.closeWinChoice()
		
		if self.choiceSide.get() == 0:
			self.winDone("vous n'avez pas joué !")
		elif self.choiceSide.get() == animal:
			self.winDone("vous avez gagné !")
			self.score += 1
			self.showGamerScore()
		else:
			self.winError("vous avez perdu")
			self.showGamerScore()
		
	def closeWinChoice(self):
		try:
			self.winChoice.destroy()
		except:
			pass
		return
		
	def drawInterface(self):
		# set menu
		menubar = Menu(self.tkRoot)
		
		# file
		mFile = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Fichier", menu=mFile)
		mFile.add_command(label="Quittez", command=self.tkRoot.destroy, accelerator="Ctrl+Q")
		
		# tools
		mPlay = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Jouer", menu=mPlay)
		mPlay.add_command(label="Arrêter", command=self.stopAnimalAnimation)
		mPlay.add_separator()
		
		mDynamic = Menu(menubar, tearoff=0)
		mPlay.add_cascade(label="Jouer Dynamique", menu=mDynamic)
		mDynamic.add_command(label="Jouer avec 1 animaux", command=lambda:self.playAnimals(animals=1))
		mDynamic.add_command(label="Jouer avec 2 animaux", command=lambda:self.playAnimals(animals=2))
		mDynamic.add_command(label="Jouer avec 3 animaux", command=lambda:self.playAnimals(animals=3))
		mDynamic.add_command(label="Jouer avec 5 animaux", command=lambda:self.playAnimals(animals=5))
		mDynamic.add_command(label="Jouer avec 7 animaux", command=lambda:self.playAnimals(animals=7))
		mDynamic.add_command(label="Jouer avec 9 animaux", command=lambda:self.playAnimals(animals=9))
		mDynamic.add_command(label="Jouer avec 11 animaux", command=lambda:self.playAnimals(animals=11))
		mDynamic.add_command(label="Jouer avec 13 animaux", command=lambda:self.playAnimals(animals=13))
		mDynamic.add_command(label="Jouer avec 15 animaux", command=lambda:self.playAnimals(animals=15))
		mDynamic.add_command(label="Jouer avec 17 animaux", command=lambda:self.playAnimals(animals=17))
		mDynamic.add_command(label="Jouer avec 19 animaux", command=lambda:self.playAnimals(animals=19))
		mDynamic.add_command(label="Jouer avec 21 animaux", command=lambda:self.playAnimals(animals=21))
		mDynamic.add_command(label="Jouer avec 24 animaux", command=lambda:self.playAnimals(animals=24))
		
		mStatic = Menu(menubar, tearoff=0)
		mPlay.add_cascade(label="Jouer Static", menu=mStatic)
		mStatic.add_command(label="Jouer avec 1 animaux", command=lambda:self.playAnimals(animals=1, static=1))
		mStatic.add_command(label="Jouer avec 2 animaux", command=lambda:self.playAnimals(animals=2, static=1))
		mStatic.add_command(label="Jouer avec 3 animaux", command=lambda:self.playAnimals(animals=3, static=1))
		mStatic.add_command(label="Jouer avec 5 animaux", command=lambda:self.playAnimals(animals=5, static=1))
		mStatic.add_command(label="Jouer avec 7 animaux", command=lambda:self.playAnimals(animals=7, static=1))
		mStatic.add_command(label="Jouer avec 9 animaux", command=lambda:self.playAnimals(animals=9, static=1))
		mStatic.add_command(label="Jouer avec 11 animaux", command=lambda:self.playAnimals(animals=11, static=1))
		mStatic.add_command(label="Jouer avec 13 animaux", command=lambda:self.playAnimals(animals=13, static=1))
		mStatic.add_command(label="Jouer avec 15 animaux", command=lambda:self.playAnimals(animals=15, static=1))
		mStatic.add_command(label="Jouer avec 17 animaux", command=lambda:self.playAnimals(animals=17, static=1))
		mStatic.add_command(label="Jouer avec 19 animaux", command=lambda:self.playAnimals(animals=19, static=1))
		mStatic.add_command(label="Jouer avec 21 animaux", command=lambda:self.playAnimals(animals=21, static=1))
		mStatic.add_command(label="Jouer avec 24 animaux", command=lambda:self.playAnimals(animals=24, static=1))
		
		# tools
		mTools = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="Outils", menu=mTools)
		mTools.add_command(label="paramètres animation", command=self.drawWinParams)
		mTools.add_command(label="paramètres écran", command=self.drawWinCanSizes)
		# about
		mAbout = Menu(menubar, tearoff=0)
		menubar.add_cascade(label="About", menu=mAbout)
		mAbout.add_command(label="Author", command=self.about)
		
		# set menu
		self.tkRoot.configure(menu=menubar) 
		
		# set shortcuts for menu
		mainShortcuts = {
			"<Control-s>":		self.stopAnimalAnimation,
			"<Control-q>":		self.quitInterface
		}
		for event, handler in mainShortcuts.items():
			self.tkRoot.bind_all(event, handler)	
			
		self.mainFrame = Frame(self.tkRoot)
		
		# widget parameters
		
		self.scoreLabel = Label(self.mainFrame, font=self.customFontBold, text="score")
		
		# image grid
		self.imageFrame = Frame(self.mainFrame)
		
		self.setMaxCanvasSize() # check the size of the screen and resize if small
		
		self.canvasPreview = Canvas(
			self.imageFrame, width=self.canvasSize[0], height=self.canvasSize[1], borderwidth=1, 
			background="#ffffff"
		)
		
		# imageframe
		imageFrameGrid = [	
			# widget							r  c  rs cs sticky
			[self.canvasPreview, 				0, 0, 1, 1, N+W+E+S]
		]
		for w in imageFrameGrid:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
		
		mainFrame = [	
			# grid representation r: row, c: column, rs: rowspan, cs: columnspan
			# widget							r  c  rs cs sticky
			[self.scoreLabel, 					0, 0, 1, 1, N+W+E+S],
			[self.imageFrame, 					1, 0, 1, 1, N+W+E+S]
		]
		
		for w in mainFrame:
			w[0].grid(row=w[1], column=w[2], rowspan=w[3], columnspan=w[4], sticky=w[5])
		
		self.mainFrame.grid(row=0, column=0, rowspan=1, columnspan=1, sticky=N+W)
		
		self.tkRoot.grid_rowconfigure(0, weight=1)
		self.mainFrame.grid_rowconfigure(0, weight=1)
		self.imageFrame.grid_rowconfigure(0, weight=1)
		
if __name__ == "__main__":
	print NAME,VERSION
	print "\n".join([AUTHOR,COPYRIGHT,URL])
	o = Interface()
	del o	
